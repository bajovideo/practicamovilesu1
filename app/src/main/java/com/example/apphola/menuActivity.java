package com.example.apphola;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class menuActivity extends AppCompatActivity {

    private static final String TAG = "menuActivity";

    private CardView crvHola, crvImc, crvConversiones, crvCambio, crvCotizacion, crvSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_menu);
        iniciarComponentes();

        // Verificar si los CardView no son nulos
        if (crvHola == null || crvImc == null || crvConversiones == null || crvCambio == null || crvCotizacion == null || crvSpinner == null) {
            Log.e(TAG, "Uno o más CardView son nulos");
            return;
        }

        crvHola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "CardView crvHola clickeado");
                Intent intent = new Intent(menuActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        crvImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "CardView crvImc clickeado");
                Intent intent = new Intent(menuActivity.this, imcActivity.class);
                startActivity(intent);
            }
        });

        crvConversiones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "CardView crvConversiones clickeado");
                Intent intent = new Intent(menuActivity.this, gradosActivity.class);
                startActivity(intent);
            }
        });

        crvCambio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "CardView crvCambio clickeado");
                Intent intent = new Intent(menuActivity.this, monedaActivity.class);
                startActivity(intent);
            }
        });
        crvCotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, ingresaCotizacionActivity.class);
                startActivity(intent);
            }
        });

        crvSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity.this, SpinnerActivity.class);
                startActivity(intent);
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes() {
        crvHola = findViewById(R.id.crvHola);
        crvImc = findViewById(R.id.crvImc);
        crvConversiones = findViewById(R.id.crvConversiones);
        crvCambio = findViewById(R.id.crvCambio);
        crvCotizacion = findViewById(R.id.crvCotizacion);
        crvSpinner = findViewById(R.id.crvSpinner);
    }
}
