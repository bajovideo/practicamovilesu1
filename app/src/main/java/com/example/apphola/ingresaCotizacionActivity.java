package com.example.apphola;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

public class ingresaCotizacionActivity extends AppCompatActivity {
    private EditText txtUsuario;
    private Button btnIngresar, btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_ingresa_cotizacion);

        // Inicializar componentes
        txtUsuario = findViewById(R.id.txtUsuario);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnCerrar = findViewById(R.id.btnCerrar);

        // Configurar el botón de ingreso
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtUsuario.getText().toString().isEmpty()) {
                    Toast.makeText(ingresaCotizacionActivity.this,
                            "Falto capturar el usuario", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(ingresaCotizacionActivity.this, CotizacionActivity.class);
                    intent.putExtra("cliente", txtUsuario.getText().toString());
                    startActivity(intent);
                }
            }
        });

        // Configurar el botón de cerrar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
