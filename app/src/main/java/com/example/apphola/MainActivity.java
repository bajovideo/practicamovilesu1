
package com.example.apphola;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {
        private TextView txtmensaje;

    private EditText txtnombre;

    private Button btnSaludar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnSaludar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //validar

                if(txtnombre.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this, "Falto capturar el nombre",Toast.LENGTH_SHORT).show();
                } else txtmensaje.setText((txtnombre.getText().toString()));
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
    public void iniciarComponentes(){

        txtmensaje = (TextView) findViewById(R.id.txtSalida);
        txtnombre = (EditText) findViewById(R.id.txtNombre);
        btnSaludar = (Button) findViewById(R.id.btnSaludar);
    }
}