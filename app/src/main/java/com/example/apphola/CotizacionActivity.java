package com.example.apphola;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CotizacionActivity extends AppCompatActivity {

    private TextView txtUsuario, txtPagoInicial, txtMensual, txtFolio;
    private EditText txtDescripcion, txtValor, txtPorcentaje;
    private RadioGroup rdgMeses;
    private RadioButton rdb12, rdb18, rdb24, rdb36;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private int folio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cotizacion);
        iniciarComponentes();

        // Generar y mostrar el folio
        Cotizacion cotizacion = new Cotizacion();
        folio = cotizacion.generarId();
        if (folio < 0) {
            folio = -folio; // Asegurar que el folio sea positivo
        }
        txtFolio.setText(String.format("Folio: %d", folio));

        // Recibir la variable de cliente desde ingresaCotizacion.java
        Intent intent = getIntent();
        String cliente = intent.getStringExtra("cliente");
        // Asignar el texto al TextView
        if (cliente != null) {
            txtUsuario.setText(cliente);
        } else {
            Toast.makeText(CotizacionActivity.this, "No se recibió el usuario", Toast.LENGTH_SHORT).show();
        }

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtDescripcion.getText().toString().matches("") ||
                        txtValor.getText().toString().matches("") ||
                        txtPorcentaje.getText().toString().matches("") ||
                        rdgMeses.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(getApplicationContext(), "FALTAN AGREGAR DATOS", Toast.LENGTH_SHORT).show();
                } else {
                    calcular();
                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtDescripcion.setText("");
                txtValor.setText("");
                txtPorcentaje.setText("");
                rdgMeses.clearCheck();
                txtPagoInicial.setText("");
                txtMensual.setText("");
            }
        });
    }

    public void iniciarComponentes() {
        txtUsuario = findViewById(R.id.txtUsuario);
        txtPagoInicial = findViewById(R.id.txtPagoInicial);
        txtMensual = findViewById(R.id.txtMensual);
        txtFolio = findViewById(R.id.txtFolio);

        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtValor = findViewById(R.id.txtValor);
        txtPorcentaje = findViewById(R.id.txtPorcentaje);

        rdgMeses = findViewById(R.id.rdgMeses);
        rdb12 = findViewById(R.id.rdb12);
        rdb18 = findViewById(R.id.rdb18);
        rdb24 = findViewById(R.id.rdb24);
        rdb36 = findViewById(R.id.rdb36);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);
    }

    private void calcular() {
        String descripcion = txtDescripcion.getText().toString();
        float valorAuto = Float.parseFloat(txtValor.getText().toString());
        float porcentajePagoInicial = Float.parseFloat(txtPorcentaje.getText().toString());
        int plazos = 0;

        if (rdgMeses.getCheckedRadioButtonId() == R.id.rdb12) {
            plazos = 12;
        } else if (rdgMeses.getCheckedRadioButtonId() == R.id.rdb18) {
            plazos = 18;
        } else if (rdgMeses.getCheckedRadioButtonId() == R.id.rdb24) {
            plazos = 24;
        } else if (rdgMeses.getCheckedRadioButtonId() == R.id.rdb36) {
            plazos = 36;
        }

        Cotizacion cotizacion = new Cotizacion(folio, descripcion, valorAuto, porcentajePagoInicial, plazos);

        float pagoInicial = cotizacion.calcularPagoInicial();
        float pagoMensual = cotizacion.calcularPagoMensual();

        txtPagoInicial.setText(String.format("Pago Inicial: %.2f", pagoInicial));
        txtMensual.setText(String.format("Pago Mensual: %.2f", pagoMensual));
    }
}
