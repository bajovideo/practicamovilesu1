package com.example.apphola;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class gradosActivity extends AppCompatActivity {
    private EditText txtGrados;
    private TextView txtResultado;
    private RadioButton rbCelsius, rbFahrenheit;
    private Button btnCalcular,btnLimpiar,btnCerar;
    private RadioGroup rgGrados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_grados);
        iniciarComponentes();
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtGrados.getText().toString().matches("") ||
                        rgGrados.getCheckedRadioButtonId() == -1) {

                    Toast.makeText(getApplicationContext(), "falto informacion", Toast.LENGTH_SHORT).show();
                } else {
                    float ga = Float.parseFloat(txtGrados.getText().toString());
                    boolean valirb = valiRb();
                    float conver = conversiones(valirb,ga);
                    txtResultado.setText(String.valueOf(conver));
                }
            }

        });

        btnCerar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }

        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtResultado.setText("");
                txtGrados.setText("");
                rgGrados.clearCheck();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
       });
    }

    private boolean valiRb() {
        if (rbCelsius.isChecked()) {
            return false;
        } else{
            return true;
        }
    }

    private float conversiones(boolean vali, float grados){
        if (vali){
            return (grados - 32)* 5/9;
        }
        else {
            return (grados * 9/5) + 32;
        }
    }
    private void iniciarComponentes() {
        txtGrados = (EditText) findViewById(R.id.txtGrados);
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnCerar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        rbCelsius = (RadioButton) findViewById(R.id.rbCelsius);
        rbFahrenheit = (RadioButton) findViewById(R.id.rbFahrenheit);
        rgGrados = (RadioGroup)  findViewById(R.id.rgGrados);

    }

}